from bs4 import BeautifulSoup
import requests

# User input
url = ''
text_to_check = ''
format_to_check='.pdf'

section_list=[]
book_list=[]

page = requests.get(url).text
soup = BeautifulSoup(page, 'html.parser')

for a in soup.find_all('a', href=True):
    dir_name=a['href']
    if(text_to_check in dir_name):
        section_list.append(dir_name)

def add_url_prefix(a):
    return url+a

url_list = list(map(add_url_prefix, section_list))

for elem in url_list:
    page = requests.get(elem).text
    soup = BeautifulSoup(page, 'html.parser')

    for a in soup.find_all('a', href=True):
        dir_name=a['href']
        if(format_to_check in dir_name):
            book_list.append(elem+dir_name)


#assumes ./pdf dir exists
for book in book_list:
    file_name = book.rsplit('/', 1)[1]
    r = requests.get(book, allow_redirects=True)
    open('pdf/' + file_name, 'wb').write(r.content)